# Untited Visualizer
A sound based microphone input visualizer written in processing langrage format.
See [exsample](youtube.com/watch?v=f3tQN1Thvqc) and [view orginal source code](https://pastebin.com/vTvvemtV).

## Created by Goldmaster for DJs by DJs
Pull requests are welcome and DJs are welcome to use Untited Visualizer for their DJ sets.

````
Please make sure minim sound libary is installed before running.
````

Processing is a program that can be downloaded from [processing.org](processing.org) and no installation is needed.


## Usage

## When launching press w or r on your keybourd to toogle between white or random.
w is for white solid dots only, while r is for random colour dots along with random transparency

Please read below

````processing
//MAIN SETUP
````
change screen size by changing 1920 and 1080 as these control screen width and hight in pixels.
````
size(1920, 1080, P3D);
````
change how long the dots take to disapper by changing the default 50 value.
````
void draw () {
  float f = random(50); //random time for dots to disapper
  fill(0, f);  //time for how quick dots disapper
````

To get this to load for a DJ set, [OBS](https://obsproject.com/) is needed. Run Untited Visualizer and in the bottom of OBS, add new source, and select Processing as the window. It is also recormended to uncheck the cursor option.

# To toggle between white and random colours make sure you click onto the application window for the keybourd buttons to work.

## Have fun using this with your DJ sets

## Goldmaster
