// Untitled Visualizer
// Coded by Goldmaster for Goldmaster Corporation
// gitlab.com/goldmaster
// idea from youtube.com/watch?v=f3tQN1Thvqc

// Note: You Will need the Minim Sound Libary added to make this work. 

//VARIABLES 
float n;
float n3;
float n5;
float speed2;

////MUSIC file importing
//import ddf.minim.*;
//import ddf.minim.signals.*;
//Minim minim;
//AudioPlayer in;

// Microphone audio input (make sure mic and settings are working)
import ddf.minim.*;
Minim minim;
AudioInput in;



//MAIN SETUP
void setup () {
  //fullScreen(P3D);
  size(1920, 1080, P3D);
  noCursor();
  smooth(100);
  background(0);

  //MUSIC | Add track to file and change name of "whatever is in qurotes bellow" to your file name. aif, wav and flac will work.
  //  minim = new Minim(this);
  //  mySound = minim.loadFile("Goldmaster - Something Like This.aif");     
  //  mySound.play();
  //}

  // 
  minim = new Minim(this);

  // use the getLineIn method of the Minim object to get an AudioInput
  in = minim.getLineIn();
}


void draw () {
  float f = random(50); //random time for dots to disapper
  fill(0, f);  //time for how quick dots disapper
  noStroke();
  rect(0, 0, width, height);
  translate(width/2, height/2); //postion of swirl point


  //control size of center
  for (int i = 0; i < in.bufferSize() -1; i++) { 
    float t = random(100); //random thickness of circles
    float angle = sin(i+(n-200))*t; 
    float z = sin(radians(i))*(n/angle); 



    float leftLevel = in.left.level() * t; 
    ellipse(i, i, leftLevel, leftLevel);
    float s = random(5);
    rotateZ(n-PI/50*20); //control rotation and control oriatation


    boolean j = true; 
    if (key=='w') { 
      fill(255, 255, 255, 255);
    } 
    j = false; 
    if (key=='r') { 
      float r = random(255);
      float g = random(255);
      float b = random(255);
      float a = random(255);


      fill(r, g, b, a); // can be changed to any colour for swirl but leave as is for random option.
    }
  }


  n += 0.008 ;
  n3 += speed2;
  n5 += speed2;
}

void keyPressed() {
  String filename = "UntitledVisualizer";
  int d = day();
  int mon = month();
  int y = year();
  int h = hour();
  int min = minute();
  int sec = second();

  String days= String.valueOf(d);
  String month= String.valueOf(mon);
  String hours= String.valueOf(h);
  String minute= String.valueOf(min);
  String year= String.valueOf(y);
  String second= String.valueOf(sec);

  if (key=='s') {
    save(year +"-"+ month  +"-"+ days +" "+ hours  +"."+ minute  +"."+ second +" - "+ filename+".jpg");
  }
}
